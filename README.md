# Catwalk CLI

Interface en lignes de commande pour le jeu Catwalk

## Instructions d'installation

**Attention: La compilation et l'exécution de Catwalk CLI requièrent l'installation de la bibliothèque [libcatwalk](https://git.kaz.bzh/valentin/libcatwalk), version antérieure ou égale à [version 0.1 alpha](https://git.kaz.bzh/valentin/libcatwalk/releases/tag/v0.1alpha).**

1. Il faut d'abord cloner le dépôt et y accéder (faire attention à la version) :
```
$ git clone https://git.kaz.bzh/valentin/catwalk-cli.git -n -b v0.1alpha
$ cd catwalk-cli
```
2. Ensuite, il faut compiler :
```
$ make
```
3. Enfin, il faut installer :
```
$ sudo make install
```

## Utilisation

Une aide précise est disponible sur le [wiki](https://git.kaz.bzh/valentin/catwalk-cli/wiki/).

## Désinstallation

Pour désinstaller, il faut exécuter
```
$ sudo make uninstall
```