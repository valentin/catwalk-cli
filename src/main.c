/* CATWALK - Test your logic
   Copyright (C) 2021 Valentin Moguerou
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE. See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>  */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <catwalk/grid.h>

#include "interact.h"
#include "simplegen.h"

#define PROGRAM_VERSION "Alpha 0.1"

void help(char *program_name)
{
    printf("\
Usage : %s [parameters]\n",
    program_name);
    puts("\n\
  -h, --help              Print this help.\n\
  -v, --version           Print version info.\n\
  -i, --interactive       Launch catwalk-cli in interactive mode (default\n\
  -s, --simple            Create a grid, without permitting to play.\n\
  -w, --width <width>     Create a grid with the given width.\n\
--interactive, --simple, --help and --version are mutually exclusive: the last argument is kept.\n\n\
This program was made with love by Valentin Moguerou <valentin@kaz.bzh>.");
}

void version()
{
    puts("Catwalk CLI version alpha 0.1\n\
Copyright (C) 2021  Valentin Moguerou <valentin@kaz.bzh>\n\
License GPLv3+ : GNU GPL version 3 or later\n\
<https://gnu.org/licenses/gpl.html>\n\
This program comes with ABSOLUTELY NO WARRANTY.\n\
This is free software, and you are welcome to redistribute it under certain conditions.");
}

int main(int argc, char **argv)
{
    int width = 4;
    char mode = 'i';
    for (int i=1; i<argc; i++)
    {
        if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
        {
            mode = 'h';
        }
        else if (strcmp(argv[i], "-i") == 0 || strcmp(argv[i], "--interactive") == 0)
        {
            mode = 'i';
        }
        else if (strcmp(argv[i], "-s") == 0 || strcmp(argv[i], "--simple") == 0)
        {
            mode = 's';
        }
        else if (strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--version") == 0)
        {
            mode = 'v';
        }
        else if (strcmp(argv[i], "-w") == 0 || strcmp(argv[i], "--width") == 0)
        {
            if (i+1==argc)
            {
                fprintf(stderr, "Please do specify a width.\n");
                exit(EXIT_FAILURE);
            }
            else
            {
                width = strtol(argv[i+1], NULL, 10); // base 10
                if (width<2)
                {
                    fprintf(stderr, "Width is too low (<2).\n");
                    exit(EXIT_FAILURE);
                }
            }
        }
    }

    srand(time(NULL));

    switch (mode)
    {
        case 'i': interact(width); break;
        case 'h': help(argv[0]); break;
        case 's': simple_generation(width); break;
        case 'v': version(); break;
    }
    return 0;
}
