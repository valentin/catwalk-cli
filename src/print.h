/* CATWALK - Test your logic
   Copyright (C) 2021 Valentin Moguerou
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE. See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>  */

#ifndef PRINT_H_INCLUDED
#define PRINT_H_INCLUDED

#include <catwalk/grid.h>
#include <catwalk/route.h>

void print_interactive_grid(grid *gd);
void print_simple_grid(grid *gd);
void print_indicators(grid *gd);
void print_route(route *rt);

#define GRID_CELL_H 3
#define GRID_CELL_W 7

#define CELL_FILL_CHAR ' '
#define CELL_NODE_CHAR '+'
#define HO_BORDER_CHAR '-'
#define VE_BORDER_CHAR '|'

#endif /* PRINT_H_INCLUDED */
