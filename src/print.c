/* CATWALK - Test your logic
   Copyright (C) 2021 Valentin Moguerou
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE. See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>  */

#include <stdio.h>

#include <catwalk/grid.h>
#include <catwalk/route.h>

#include "print.h"

//#define  COLORED_PATH
#ifdef   COLORED_PATH
# define KNRM "\e[0m"
# define KRED "\e[31m"
#endif

const char ASCII_START[GRID_CELL_H][GRID_CELL_W]
= {
    {' ', ' ', '/',  '\\', ' ',  ' ', ' '},
    {' ', '/', '\\', ' ',  '\\', '/', ' '},
    {' ', ' ', ' ',  '\\', '/',  ' ', ' '}
};

const char ASCII_CAT[GRID_CELL_H][GRID_CELL_W]
= {
    {' ', '/', '\\', '_', '/', '\\', ' '},
    {'(', ' ', 'o',  '.', 'o', ' ',  ')'},
    {' ', '>', ' ',  '^', ' ', '<',  ' '}
};

const char ASCII_MILK[GRID_CELL_H][GRID_CELL_W]
= {
    {' ', ' ',  '=', '=', ' ',  ' ', ' '},
    {' ', '/',  'm', 'i', '\\', ' ', ' '},
    {' ', '\\', 'l', 'k', '/',  ' ', ' '}
};

void print_interactive_grid(grid *gd)
{
    element *pos = gd->player_route->last; // define pointer to last element of player route
    element *el;
    for (int y=0; y<(gd->width+1)*(GRID_CELL_H+1); y++) // for each line
    {
        for (int x=0; x<(gd->width+1)*(GRID_CELL_W+1); x++) // for each column
        {
            if (x<GRID_CELL_W || y<GRID_CELL_H) // if header (indicators)
            {
                if (x == GRID_CELL_W/2 && y == GRID_CELL_H/2) // top left blank space
                {
                    putchar(CELL_FILL_CHAR);
                }
                else if (x%(GRID_CELL_W+1) == GRID_CELL_W/2 && y == GRID_CELL_H/2) // top indicators
                {
                    printf("%d", gd->hints->x[x/(GRID_CELL_W+1)-1]);
                }
                else if (x == GRID_CELL_W/2 && y%(GRID_CELL_H+1) == GRID_CELL_H/2) // left indicators
                {
                    printf("%d", gd->hints->y[y/(GRID_CELL_H+1)-1]);
                }
                else
                {
                    putchar(CELL_FILL_CHAR); // blank space around numbers
                }
            }
            else // else if body (cells)
            {
                if ((x+1)%(GRID_CELL_W+1) == 0 && (y+1)%(GRID_CELL_H+1) == 0)
                {
                    putchar(CELL_NODE_CHAR); // cell border intersections
                }
                else if ((y+1)%(GRID_CELL_H+1) == 0)
                {
                    putchar(HO_BORDER_CHAR); // cell horizontal borders
                }
                else if ((x+1)%(GRID_CELL_W+1) == 0)
                {
                    putchar(VE_BORDER_CHAR); // cell vertical borders
                }
                else // if inside cells
                {
                    if (x/(GRID_CELL_W+1)-1 == pos->x && y/(GRID_CELL_H+1)-1 == pos->y) // if cursor position corresponds to the player position
                    {
                        putchar(ASCII_CAT[y%(GRID_CELL_H+1)][x%(GRID_CELL_W+1)]);
                    }
                    else if (x/(GRID_CELL_W+1)-1 == gd->start[0] && y/(GRID_CELL_H+1)-1 == gd->start[1]) // if the player has moved, if cursor position corresponds to the cat position
                    {
                        putchar(ASCII_START[y%(GRID_CELL_H+1)][x%(GRID_CELL_W+1)]);
                    }
                    else if (x/(GRID_CELL_W+1)-1 == gd->end[0] && y/(GRID_CELL_H+1)-1 == gd->end[1]) // if corresponds to the milk position
                    {
                        putchar(ASCII_MILK[y%(GRID_CELL_H+1)][x%(GRID_CELL_W+1)]);
                    }
                    else if (!is_free(gd->player_route, x/(GRID_CELL_W+1)-1, y/(GRID_CELL_H+1)-1))
                    {
                        if (!el)
                        {
                            el = find_coordinates(gd->player_route, x/(GRID_CELL_W+1)-1, y/(GRID_CELL_H+1)-1);
                        }
                        else if (el->x!=x/(GRID_CELL_W+1)-1 || el->y!=y/(GRID_CELL_H+1)-1)
                        {
                            el = find_coordinates(gd->player_route, x/(GRID_CELL_W+1)-1, y/(GRID_CELL_H+1)-1);
                        }
                        if (x%(GRID_CELL_W+1) == GRID_CELL_W/2 && y%(GRID_CELL_H+1) == GRID_CELL_H/2)
                        {
                            #ifdef COLORED_PATH
                            printf("%s%c%s", KRED, CELL_NODE_CHAR, KNRM);
                            #else
                            putchar(CELL_NODE_CHAR);
                            #endif
                        }
                        else if (x%(GRID_CELL_W+1) == GRID_CELL_W/2 || y%(GRID_CELL_H+1) == GRID_CELL_H/2)
                        {
                            if (x%(GRID_CELL_W+1) < GRID_CELL_W/2 && is_left(el, el->previous))
                            {
                                #ifdef COLORED_PATH
                                printf("%s%c%s", KRED, HO_BORDER_CHAR, KNRM);
                                #else
                                putchar(HO_BORDER_CHAR);
                                #endif
                            }
                            else if (x%(GRID_CELL_W+1) > GRID_CELL_W/2 && is_right(el, el->previous))
                            {
                                #ifdef COLORED_PATH
                                printf("%s%c%s", KRED, HO_BORDER_CHAR, KNRM);
                                #else
                                putchar(HO_BORDER_CHAR);
                                #endif
                            }
                            else if (y%(GRID_CELL_H+1) < GRID_CELL_H/2 && is_up(el, el->previous))
                            {
                                #ifdef COLORED_PATH
                                printf("%s%c%s", KRED, VE_BORDER_CHAR, KNRM);
                                #else
                                putchar(VE_BORDER_CHAR);
                                #endif
                            }
                            else if (y%(GRID_CELL_H+1) > GRID_CELL_H/2 && is_down(el, el->previous))
                            {
                                #ifdef COLORED_PATH
                                printf("%s%c%s", KRED, VE_BORDER_CHAR, KNRM);
                                #else
                                putchar(VE_BORDER_CHAR);
                                #endif
                            }
                            else if (x%(GRID_CELL_W+1) < GRID_CELL_W/2 && is_left(el, el->next))
                            {
                                #ifdef COLORED_PATH
                                printf("%s%c%s", KRED, HO_BORDER_CHAR, KNRM);
                                #else
                                putchar(HO_BORDER_CHAR);
                                #endif
                            }
                            else if (x%(GRID_CELL_W+1) > GRID_CELL_W/2 && is_right(el, el->next))
                            {
                                #ifdef COLORED_PATH
                                printf("%s%c%s", KRED, HO_BORDER_CHAR, KNRM);
                                #else
                                putchar(HO_BORDER_CHAR);
                                #endif
                            }
                            else if (y%(GRID_CELL_H+1) < GRID_CELL_H/2 && is_up(el, el->next))
                            {
                                #ifdef COLORED_PATH
                                printf("%s%c%s", KRED, VE_BORDER_CHAR, KNRM);
                                #else
                                putchar(VE_BORDER_CHAR);
                                #endif
                            }
                            else if (y%(GRID_CELL_H+1) > GRID_CELL_H/2 && is_down(el, el->next))
                            {
                                #ifdef COLORED_PATH
                                printf("%s%c%s", KRED, VE_BORDER_CHAR, KNRM);
                                #else
                                putchar(VE_BORDER_CHAR);
                                #endif
                            }
                            else
                            {
                                putchar(CELL_FILL_CHAR);
                            }
                        }
                        else
                        {
                            putchar(CELL_FILL_CHAR);
                        }
                    }
                    else // if cursor position corresponds to nothing
                    {
                        putchar(CELL_FILL_CHAR);
                    }
                }
            }
        }
        putchar('\n'); // newline
    }
}


void print_simple_grid(grid *gd)
{
    for (int y=0; y<(gd->width+1)*(GRID_CELL_H+1); y++) // for each line
    {
        for (int x=0; x<(gd->width+1)*(GRID_CELL_W+1); x++) // for each column
        {
            if (x<GRID_CELL_W || y<GRID_CELL_H) // if header (indicators)
            {
                if (x == GRID_CELL_W/2 && y == GRID_CELL_H/2) // top left blank space
                {
                    putchar(CELL_FILL_CHAR);
                }
                else if (x%(GRID_CELL_W+1) == GRID_CELL_W/2 && y == GRID_CELL_H/2) // top indicators
                {
                    printf("%d", gd->hints->x[x/(GRID_CELL_W+1)-1]);
                }
                else if (x == GRID_CELL_W/2 && y%(GRID_CELL_H+1) == GRID_CELL_H/2) // left indicators
                {
                    printf("%d", gd->hints->y[y/(GRID_CELL_H+1)-1]);
                }
                else
                {
                    putchar(CELL_FILL_CHAR); // blank space around numbers
                }
            }
            else // else if body (cells)
            {
                if ((x+1)%(GRID_CELL_W+1) == 0 && (y+1)%(GRID_CELL_H+1) == 0)
                {
                    putchar(CELL_NODE_CHAR); // cell border intersections
                }
                else if ((y+1)%(GRID_CELL_H+1) == 0)
                {
                    putchar(HO_BORDER_CHAR); // cell horizontal borders
                }
                else if ((x+1)%(GRID_CELL_W+1) == 0)
                {
                    putchar(VE_BORDER_CHAR); // cell vertical borders
                }
                else // if inside cells
                {
                    if (x/(GRID_CELL_W+1)-1 == gd->start[0] && y/(GRID_CELL_H+1)-1 == gd->start[1])
                    {
                        putchar(ASCII_CAT[y%(GRID_CELL_H+1)][x%(GRID_CELL_W+1)]);
                    }
                    else if (x/(GRID_CELL_W+1)-1 == gd->end[0] && y/(GRID_CELL_H+1)-1 == gd->end[1])
                    {
                        putchar(ASCII_MILK[y%(GRID_CELL_H+1)][x%(GRID_CELL_W+1)]);
                    }
                    else
                    {
                        putchar(CELL_FILL_CHAR);
                    }
                }
            }
        }
        putchar('\n'); // newline
    }
}

void print_indicators(grid *gd)
{
    printf("Indicators :\nX: ");
    int i;
    for (i=0; i<gd->width; i++)
    {
        printf("%d ", gd->hints->x[i]);
    }
    printf("\nY: ");
    for (i=0; i<gd->width; i++)
    {
        printf("%d ", gd->hints->y[i]);
    }
    printf("\n");
}

void print_route(route *rt)
{
    element *el = rt->first;
    printf("[(%d, %d)", el->x, el->y);
    el = el->next;
    while (el)
    {
        printf(", (%d, %d)", el->x, el->y);
        el = el->next;
    }
    printf("]\n");
}
