CC	= gcc
FLAGS	= -Wall -g
LFLAGS	= -L/usr/local/lib/ -lcatwalk

all: bin/catwalk

bin/catwalk: obj/main.o obj/interact.o obj/print.o obj/simplegen.o
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -o $@ $^ $(LFLAGS)

obj/%.o: src/%.c
	@mkdir -p $(@D)
	$(CC) $(FLAGS) -o $@ -c $<

obj/main.o: src/main.c
obj/interact.o: src/interact.c
obj/print.o: src/print.c
obj/simplegen.o: src/simplegen.c

.PHONY: clean mrproper install
clean:
	rm -rf obj
mrproper: clean
	rm -rf bin

install: bin/catwalk
	@mkdir -p /usr/local/bin/
	install -p bin/catwalk /usr/local/bin/

uninstall:
	rm -f /usr/local/bin/catwalk